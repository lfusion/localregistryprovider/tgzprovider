﻿# LFusion Tgz Provider

Ce package est une extension pour LFusion Local Registry Provider.  
Il permet de référencer les paquets tgz stockés dans le projet, localement ou en ligne.  
Il contient également des classes et des helpers pour simplifier l'implémentation de providers.  

 1. [Prérequis](#prérequis)
 2. [Installation](#installation)
 3. [Configuration](#configuration)
 4. [ITgzProvider](#itgzprovider)

## Prérequis

 - ***Unity 2020*** ou supérieur
 - ***LFusion Local Registry Provider*** : `com.lfusion.localregistryprovider.core`

## Installation

### Installer via Gitlab registry

 - Ajouter le registre [LFusion Upm-Registry](https://gitlab.com/lfusion/upm-registry)
 - **Pour Unity 2021 ou plus récent**
	 - Package Manager
	 - **\+** Add package by name...
		 - `Name : com.lfusion.localregistryprovider.tgzprovider`
 - **Pour toutes les versions supportées**
	 - Ouvrir `manifest.json` dans le dossier Packages du projet.
	 - Ajouter `"com.lfusion.localregistryprovider.tgzprovider": "x.x.x",` dans le bloc `dependencies`  en remplaçant `x.x.x` par la version voulu.
	 - Exemple pour la version `1.0.0` :
```json
{
  "dependencies": {
    "com.lfusion.localregistryprovider.tgzprovider": "1.0.0",
    [...]
  },
  [...]
}
```

### Installer via tarball

 - Télécharger `com.lfusion.localregistryprovider.tgzprovider-x.x.x.tgz` depuis [releases](https://gitlab.com/lfusion/localregistryprovider/tgzprovider/-/releases).
 - Package Manager
 - **\+** Add package from tarball...
 - Sélectionnez  `com.lfusion.localregistryprovider.tgzprovider-x.x.x.tgz`

## Configuration

Les paramètres de LFusion Tgz Provider sont accessibles depuis:  

 1. `Edit/Project Settings...`
 2. `Package Manager/Local Registry/Tgz Provider`

### Packages

Gestion des packages tarball référencés manuellement.

| Nom                   | Description                                           |
|-----------------------|-------------------------------------------------------|
| Apply Now             | Force la mise à jour des packages listés              |
| **+**                 | Ajoute une ligne                                      |
| **-**                 | Supprime la ligne sélectionnée                        |

#### Package Libraries

Chaque ligne permet de référencer un dossier contenant des packages.  
Les packages présents dans une Library sont automatiquement référencés.  

#### Individual Packages

Chaque ligne permet de référencer un package.  

| Source Type   | Description                             |
|---------------|-----------------------------------------|
| Project       | Utilise un chemin relatif au projet     |
| Local         | Utilise un chemin absolu (à éviter)     |
| Url           | Utilise un url (support http & https)   |

### Cache Manager

Permet de supprimer les informations des packages stockés en cache.  

## ITgzProvider

ITgzProvider est une classe abstraite qui simplifie l'implémentation d'un fournisseur.  
Il gère le téléchargement de package hébergé, l'utilisation des informations du package et le calcul du shasum.