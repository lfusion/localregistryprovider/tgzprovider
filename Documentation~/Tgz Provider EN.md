﻿# LFusion Tgz Provider

This package is an extension for LFusion Local Registry Provider.  
It can be used to reference tgz packages stored in the project, locally or online.  
It also contains classes and helpers to simplify the implementation of providers.  

 1. [Prerequisites](#prerequisites)
 2. [Installation](#installation)
 3. [Configuration](#configuration)
 4. [ITgzProvider](#itgzprovider)

## Prerequisites

 - ***Unity 2020*** or higher
 - ***LFusion Local Registry Provider*** : `com.lfusion.localregistryprovider.core`

## Installation

### Install via Gitlab registry

 - Add the [LFusion Upm-Registry](https://gitlab.com/lfusion/upm-registry)
 - **For Unity 2021 or highter**
	 - Package Manager
	 - **\+** Add package by name...
		 - `Name : com.lfusion.localregistryprovider.tgzprovider`
 - **For all supported Unity versions**
	 - Open `manifest.json` in the Packages folder of the project.
	 - Add `"com.lfusion.localregistryprovider.tgzprovider": "x.x.x",` to the `dependencies` block, replacing `x.x.x` with the desired version.
	 - Example for version `1.0.0` :
```json
{
  "dependencies": {
    "com.lfusion.localregistryprovider.tgzprovider": "1.0.0",
    [...]
  },
  [...]
}
```

### Install via tarball

 - Download `com.lfusion.localregistryprovider.tgzprovider-x.x.x.tgz` from [releases](https://gitlab.com/lfusion/localregistryprovider/tgzprovider/-/releases).
 - Package Manager
 - **\+** Add package from tarball...
 - Select  `com.lfusion.localregistryprovider.tgzprovider-x.x.x.tgz`

## Configuration

LFusion Tgz Provider settings can be accessed from:  

 1. `Edit/Project Settings...`
 2. `Package Manager/Local Registry/Tgz Provider`

### Packages

Management of manually referenced tarball packages.

| Name                  | Description                                           |
|-----------------------|-------------------------------------------------------|
| Apply Now             | Force update of listed packages                       |
| **+**                 | Add a line                                            |
| **-**                 | Deletes the selected line                             |

#### Package Libraries

Each line references a folder containing packages.  
Packages present in a Library are automatically referenced.  

#### Individual Packages

Each line references a package.  

| Source Type   | Description                             |
|---------------|-----------------------------------------|
| Project       | Uses a path relative to the project     |
| Local         | Uses an absolute path (avoid this)      |
| Url           | Uses a url (http & https support)       |

### Cache Manager

Used to delete cached package information.  

## ITgzProvider

ITgzProvider is an abstract class that simplifies the implementation of a provider.  
It manages the downloading of hosted packages, the use of package information and the calculation of shasum.