/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using System;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine;

	[FilePath("ProjectSettings/LFusion/TgzProviderSettings.asset", FilePathAttribute.Location.ProjectFolder)]
	public class Settings : ScriptableSingleton<Settings>
	{
		#region Fields
		[SerializeField] private List<PackageSource> m_packageSources = new List<PackageSource>();
		[SerializeField] private List<PackageLibrary> m_packageLibraries = new List<PackageLibrary>();
		#endregion Fields

		#region Properties
		public IReadOnlyList<PackageSource> PackageSources { get => m_packageSources.AsReadOnly(); }
		public IReadOnlyList<PackageLibrary> PackageLibraries { get => m_packageLibraries.AsReadOnly(); }
		#endregion Properties

		#region Events
		private Action<IReadOnlyList<PackageSource>> a_packageSourcesChanged;
		public event Action<IReadOnlyList<PackageSource>> PackageSourcesChanged
		{
			add
			{
				a_packageSourcesChanged -= value;
				a_packageSourcesChanged += value;
			}
			remove
			{
				a_packageSourcesChanged -= value;
			}
		}

		private Action<IReadOnlyList<PackageLibrary>> a_packageLibrariesChanged;
		public event Action<IReadOnlyList<PackageLibrary>> PackageLibrariesChanged
		{
			add
			{
				a_packageLibrariesChanged -= value;
				a_packageLibrariesChanged += value;
			}
			remove
			{
				a_packageLibrariesChanged -= value;
			}
		}
		#endregion Events

		#region Methods
		#region Scriptable
		private void OnEnable()
		{
			hideFlags &= ~HideFlags.NotEditable;
		}

		void OnDisable()
		{
			Save();
		}
		#endregion Scriptable

		/// <summary>
		/// Save the timeline project settings file in the project directory.
		/// </summary>
		public void Save()
		{
			Save(true);
		}

		internal void NotifyPackageSourcesChanged()
		{
			a_packageSourcesChanged?.Invoke(PackageSources);
		}

		internal void NotifyPackageLibrariesChanged()
		{
			a_packageLibrariesChanged?.Invoke(PackageLibraries);
		}

		#region Static
		public static SerializedObject GetSerialized()
		{
			return new SerializedObject(instance);
		}
		#endregion Static
		#endregion Methods
	}
}