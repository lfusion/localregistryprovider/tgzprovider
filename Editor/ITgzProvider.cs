/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using LFLRP = LFusion.LocalRegistryProvider;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Threading.Tasks;
	using LFusion.LocalRegistryProvider.TgzProvider.Helpers;

	public abstract class ITgzProvider : IProvider
	{
		#region Constantes
		private const string LOG_PREFIX = "<color=orange>[ITgzProvider]</color>";
		private const string SAVE_RELATIVE_PATH = "Library/LFusion/LocalRegisteryProvider/TgzProviderCache.json";
		private static readonly string s_cachePath;
		#endregion Constantes

		#region Fields
		private bool m_packagesHasChanged = false;
		private List<PackageInfo> m_packageInfos = new List<PackageInfo>();
		private List<PackageSource> m_packageSources = new List<PackageSource>();
		private Dictionary<PackageVersionInfo, PackageSource> m_packageVersionSources = new Dictionary<PackageVersionInfo, PackageSource>();

		#region Static
		private static Dictionary<string, PackageVersionInfo> s_cache = new Dictionary<string, PackageVersionInfo>();
		#endregion Static
		#endregion Fields

		#region Properties
		public PackageInfo[] Packages
		{
			get
			{
				return m_packageInfos.ToArray();
			}
		}

		public bool PackagesHasChanged
		{
			get => m_packagesHasChanged;
			set => m_packagesHasChanged = value;
		}

		#region Abstract
		public abstract string Name { get; }

		public abstract string Description { get; }
		#endregion Abstract

		#region Internals
		/// <summary>
		/// List of sources used to fetch packages
		/// </summary>
		protected List<PackageSource> PackageSources
		{
			get => m_packageSources;
			set => m_packageSources = value;
		}

		internal static Dictionary<string, PackageVersionInfo> Cache
		{
			get => s_cache;
		}
		#endregion Internals
		#endregion Properties

		#region Methods

		#region Constructors
		public ITgzProvider()
		{
			for (int i = 0; i < m_packageSources.Count; i++)
			{
				if (s_cache.ContainsKey(m_packageSources[i].Path))
				{
					AddPackageVersion(s_cache[m_packageSources[i].Path]);
				}
			}
		}

		static ITgzProvider()
		{
			s_cachePath = string.Format("{0}/{1}", LFLRP.Helpers.ProjectPath, SAVE_RELATIVE_PATH);

			LoadCache();
		}
		#endregion Constructors

		public string GetLocalPath(PackageVersionInfo packageVersionInfo)
		{
			if (m_packageVersionSources.ContainsKey(packageVersionInfo))
			{
				PackageSource source = m_packageVersionSources[packageVersionInfo];
				switch (source.SourceType)
				{
					case SourceType.Project:
						return string.Format("{0}/{1}", LFLRP.Helpers.ProjectPath, source.Path);
					case SourceType.Local:
						return source.Path;
					case SourceType.Http:
						break;
					default:
						break;
				}
			}

			return null;
		}

		public void Refresh(bool force = false)
		{
			RefreshSources(force);
			m_packageInfos.Clear();

			for (int i = 0; i < m_packageSources.Count; i++)
			{
				if (m_packageSources[i].IsValidePackage() == false)
				{
					continue;
				}
				string sourcePath = m_packageSources[i].Path;

				PackageVersionInfo packageVersionInfo = null;

				if (force == false && s_cache.ContainsKey(sourcePath))
				{
					packageVersionInfo = s_cache[sourcePath];
				}
				else
				{
					string json = null;
					string shasum = null;

					string url = null;

					switch (m_packageSources[i].SourceType)
					{
						case SourceType.Project:
							json = TarGz.GetPackageJsonFronTgz(Path.Combine(LFLRP.Helpers.ProjectPath, sourcePath));
							shasum = LFLRP.Helpers.ComputeShasum(Path.Combine(LFLRP.Helpers.ProjectPath, sourcePath));
							break;
						case SourceType.Local:
							json = TarGz.GetPackageJsonFronTgz(sourcePath);
							shasum = LFLRP.Helpers.ComputeShasum(sourcePath);
							break;
						case SourceType.Http:
							{
								byte[] data = Web.Get(sourcePath)?.Data;
								if (data != null)
								{
									json = TarGz.GetPackageJsonFromTgz(data);
									shasum = LFLRP.Helpers.ComputeShasum(data);
									url = sourcePath;
								}
							}
							break;
						default:
							break;
					}

					if (string.IsNullOrEmpty(json) == false)
					{
						packageVersionInfo = new PackageVersionInfo();
						packageVersionInfo.ParseJson(json);
						packageVersionInfo.Dist = new DistInfo() { Shasum = shasum, Tarball = url };


						if (s_cache.ContainsKey(sourcePath))
						{
							s_cache[sourcePath] = packageVersionInfo;
						}
						else
						{
							s_cache.Add(sourcePath, packageVersionInfo);
						}
					}
				}

				if (packageVersionInfo != null)
				{
					AddPackageVersion(packageVersionInfo);
					if (m_packageVersionSources.ContainsKey(packageVersionInfo))
					{
						m_packageVersionSources[packageVersionInfo] = m_packageSources[i];
					}
					else
					{
						m_packageVersionSources.Add(packageVersionInfo, m_packageSources[i]);
					}
				}
			}

			SaveCache();

			m_packagesHasChanged = true;

			Log("Packages list Refreshed");
		}

		public async Task RefreshAsync(bool force = false)
		{
			await RefreshSourcesAsync(force);
			m_packageInfos.Clear();

			for (int i = 0; i < m_packageSources.Count; i++)
			{
				if (m_packageSources[i].IsValidePackage() == false)
				{
					continue;
				}
				string sourcePath = m_packageSources[i].Path;

				PackageVersionInfo packageVersionInfo = null;

				if (force == false && s_cache.ContainsKey(sourcePath))
				{
					packageVersionInfo = s_cache[sourcePath];
				}
				else
				{
					string json = null;
					string shasum = null;

					string url = null;

					switch (m_packageSources[i].SourceType)
					{
						case SourceType.Project:
							json = TarGz.GetPackageJsonFronTgz(Path.Combine(LFLRP.Helpers.ProjectPath, sourcePath));
							shasum = LFLRP.Helpers.ComputeShasum(Path.Combine(LFLRP.Helpers.ProjectPath, sourcePath));
							break;
						case SourceType.Local:
							json = TarGz.GetPackageJsonFronTgz(sourcePath);
							shasum = LFLRP.Helpers.ComputeShasum(sourcePath);
							break;
						case SourceType.Http:
							{
								byte[] data = (await Web.GetAsync(sourcePath))?.Data;
								if (data != null)
								{
									json = TarGz.GetPackageJsonFromTgz(data);
									shasum = LFLRP.Helpers.ComputeShasum(data);
									url = sourcePath;
								}
							}
							break;
						default:
							break;
					}

					if (string.IsNullOrEmpty(json) == false)
					{
						packageVersionInfo = new PackageVersionInfo();
						packageVersionInfo.ParseJson(json);
						packageVersionInfo.Dist = new DistInfo() { Shasum = shasum, Tarball = url };

						if (s_cache.ContainsKey(sourcePath))
						{
							s_cache[sourcePath] = packageVersionInfo;
						}
						else
						{
							s_cache.Add(sourcePath, packageVersionInfo);
						}
					}
				}

				if (packageVersionInfo != null)
				{
					AddPackageVersion(packageVersionInfo);
					if (m_packageVersionSources.ContainsKey(packageVersionInfo))
					{
						m_packageVersionSources[packageVersionInfo] = m_packageSources[i];
					}
					else
					{
						m_packageVersionSources.Add(packageVersionInfo, m_packageSources[i]);
					}
				}
			}

			SaveCache();

			m_packagesHasChanged = true;

			Log("Packages list Refreshed");
		}

		protected void AddPackageVersion(PackageVersionInfo packageVersionInfo)
		{
			PackageInfo package = m_packageInfos.Find(x => x.Name == packageVersionInfo.Name);

			if (package == null)
			{
				package = new PackageInfo() { Name = packageVersionInfo.Name };
				m_packageInfos.Add(package);
			}

			if (package.Versions.ContainsKey(packageVersionInfo.Version) == false)
			{
				package.Versions.Add(packageVersionInfo.Version, packageVersionInfo);
			}

			string version = packageVersionInfo.Version;

			if (package.Time.ContainsKey(version))
			{
				package.Time[version] = LFLRP.Helpers.RegistryDateTimeFormat(DateTime.UtcNow);
			}
			else
			{
				package.Time.Add(version, LFLRP.Helpers.RegistryDateTimeFormat(DateTime.UtcNow));
			}
		}

		public IReadOnlyList<PackageSource> GetPackageSources()
		{
			return m_packageSources.AsReadOnly();
		}

		#region Static
		internal static void LoadCache()
		{
			if (File.Exists(s_cachePath))
			{
				string json = File.ReadAllText(s_cachePath);

				s_cache.ParseJson(json);
			}
		}

		internal static void SaveCache()
		{
			Directory.CreateDirectory(Directory.GetParent(s_cachePath).FullName);

			string json = s_cache.ToJson();

			File.WriteAllText(s_cachePath, json);
		}
		#endregion Static

		#region Abstracts
		public abstract void OpenSettings();

		/// <summary>
		/// Update <see cref="PackageSources"/>
		/// </summary>
		/// <param name="force">request to ignore cache if used</param>
		protected abstract void RefreshSources(bool force = false);

		/// <summary>
		/// Update <see cref="PackageSources"/> asynchronously 
		/// </summary>
		/// <param name="force">request to ignore cache if used</param>
		/// <returns>asynchronous task</returns>
		protected abstract Task RefreshSourcesAsync(bool force = false);
		#endregion Abstracts

		#region Debugs
		private static void Log(string log)
		{
			LocalRegistryProvider.Debug.Log(log, LOG_PREFIX);
		}

		private static void LogWarning(string log)
		{
			LocalRegistryProvider.Debug.LogWarning(log, LOG_PREFIX);
		}

		private static void LogError(string log)
		{
			LocalRegistryProvider.Debug.LogError(log, LOG_PREFIX);
		}

		#endregion Debugs

		#endregion Methods
	}
}

