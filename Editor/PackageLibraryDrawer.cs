/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using LFLRP = LFusion.LocalRegistryProvider;
	using System.IO;
	using UnityEditor;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;

	[CustomPropertyDrawer(typeof(PackageLibrary))]
	public class PackageLibraryDrawer : PropertyDrawer
	{
		#region Methods
		#region PropertyDrawer
		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			return new PackageLibraryVisualElement(property);
		}
		#endregion PropertyDrawer
		#endregion Methods
	}

	public class PackageLibraryVisualElement : VisualElement
	{
		#region Constants
		private const string IS_ABSOLUTE_NAME = "m_isAbsolutePath";
		private const string PATH_NAME = "m_path";
		#endregion Constants

		#region Fields
		private Label m_pathLabel = null;
		private PropertyField m_pathField = null;
		private Button m_selectBtn = null;
		private SerializedProperty m_property = null;
		#endregion Fields

		#region Methods
		#region PropertyDrawer
		public PackageLibraryVisualElement(SerializedProperty property)
		{
			m_property = property;

			style.flexDirection = FlexDirection.Row;
			style.marginTop = 1;

			Label labelType = new Label()
			{
				text = "Use absolute path"
			};

			StyleSheet disablePropertyLabelStyle = LFLRP.UI.Helpers.LoadDisablePropertyLabelStyle();

			PropertyField isAbsoluteField = new PropertyField(property.FindPropertyRelative(IS_ABSOLUTE_NAME));
			isAbsoluteField.style.alignSelf = Align.Center;
			isAbsoluteField.styleSheets.Add(disablePropertyLabelStyle);
			isAbsoluteField.RegisterValueChangeCallback(OnTypeChanged);

			m_pathLabel = new Label()
			{
				text = GetPathLabel(property.FindPropertyRelative(IS_ABSOLUTE_NAME).boolValue)
			};

			m_pathField = new PropertyField(property.FindPropertyRelative(PATH_NAME));
			m_pathField.styleSheets.Add(disablePropertyLabelStyle);
			m_pathField.style.flexGrow = 1;

			m_pathField.RegisterValueChangeCallback(OnPathChanged);

			m_selectBtn = new Button()
			{
				text = "Select"
			};

			m_selectBtn.clicked += OnSelectBtnClicked;

			Add(labelType);
			Add(isAbsoluteField);
			Add(m_pathLabel);
			Add(m_pathField);
			Add(m_selectBtn);
		}
		#endregion PropertyDrawer

		private string GetPathLabel(bool isAbsolute)
		{
			if (isAbsolute)
			{
				return "Absolute Path";
			}

			return "Relative Path";
		}

		private string CheckPath(bool isAbsolute, string path)
		{
			if (string.IsNullOrEmpty(path))
			{
				return "Path is empty";
			}

			if (isAbsolute == false)
			{
				path = LFLRP.Helpers.ProjectPath + "/" + path;
			}
			else if (Path.IsPathRooted(path) == false)
			{
				return "Is not an absolute Path";
			}

			if (Directory.Exists(path) == false)
			{
				return "Folder not exists";
			}

			return null;
		}

		private void UpdateUI()
		{
			bool isAbsolute = m_property.FindPropertyRelative(IS_ABSOLUTE_NAME).boolValue;
			string path = m_property.FindPropertyRelative(PATH_NAME).stringValue;
			string error = CheckPath(isAbsolute, path);

			m_pathLabel.text = GetPathLabel(isAbsolute);

			if (error != null)
			{
				AddToClassList("error");
				style.backgroundColor = new Color(1, 0, 0, 0.35f);
				tooltip = error;
			}
			else
			{
				RemoveFromClassList("error");
				style.backgroundColor = new StyleColor();
				tooltip = null;
			}
		}

		#region Callbacks
		private void OnPathChanged(SerializedPropertyChangeEvent e)
		{
			UpdateUI();

			PackageLibrary packageLibrary = LFLRP.Helpers.GetTargetObjectOfProperty(m_property) as PackageLibrary;
			packageLibrary?.UpdateTracking();
		}

		private void OnSelectBtnClicked()
		{
			string absolutePath = EditorUtility.OpenFolderPanel("Select library folder", LFLRP.Helpers.ProjectPath, "");

			if (string.IsNullOrEmpty(absolutePath))
			{
				return;
			}

			bool isAbsolute = m_property.FindPropertyRelative(IS_ABSOLUTE_NAME).boolValue;

			if (isAbsolute)
			{
				m_property.FindPropertyRelative(PATH_NAME).stringValue = absolutePath;
				m_property.serializedObject.ApplyModifiedProperties();
			}
			else
			{
				m_property.FindPropertyRelative(PATH_NAME).stringValue = absolutePath.Replace(LFLRP.Helpers.ProjectPath, "").Remove(0, 1);
				m_property.serializedObject.ApplyModifiedProperties();
			}
		}

		private void OnTypeChanged(SerializedPropertyChangeEvent e)
		{
			UpdateUI();

			PackageLibrary packageLibrary = LFLRP.Helpers.GetTargetObjectOfProperty(m_property) as PackageLibrary;
			packageLibrary?.UpdateTracking();
		}
		#endregion Callbacks
		#endregion Methods
	}
}