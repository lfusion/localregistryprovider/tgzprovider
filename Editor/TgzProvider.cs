/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using UnityEditor;
	using LFUI = LocalRegistryProvider.UI;

	public class TgzProvider : ITgzProvider
	{
		#region Properties
		public override string Name => Constants.PackageDisplayedName;
		public override string Description => Constants.Description;
		#endregion Properties

		#region Constructor
		public TgzProvider()
		{
			UpdateSourcesTracking();
			Settings.instance.PackageSourcesChanged += OnPackageSourcesChanged;
			UpdateLibrariesTracking();
			Settings.instance.PackageLibrariesChanged += OnPackageLibrariesChanged;
		}
		#endregion Constructor

		#region Methods
		#region ITgzProvider
		public override void OpenSettings()
		{
			SettingsService.OpenProjectSettings(LFUI.Helpers.SettingsPath(Constants.PackageShortDisplayedName));
		}

		protected override void RefreshSources(bool force)
		{
			List<PackageSource> packageSources = new List<PackageSource>(Settings.instance.PackageSources);
			foreach (PackageLibrary packageLibrary in Settings.instance.PackageLibraries)
			{
				PackageSource[] packagesFromLibrary = packageLibrary.GetPackages();
				if (packagesFromLibrary != null)
				{
					packageSources.AddRange(packagesFromLibrary);
				}
			}

			PackageSources = packageSources;
		}

		protected override async Task RefreshSourcesAsync(bool force)
		{
			RefreshSources(force);
			await Task.Yield();
		}
		#endregion ITgzProvider

		private void UpdateSourcesTracking()
		{
			foreach (PackageSource packageSource in Settings.instance.PackageSources)
			{
				packageSource.PackageChanged += OnPackageSourceChanged;
				packageSource.UpdateTracking();
			}
		}

		private async void OnPackageSourceChanged(PackageSource obj)
		{
			await ProviderManager.RefreshProviderAsync<TgzProvider>();
		}

		private async void UpdateLibrariesTracking()
		{
			foreach (PackageLibrary packageLibrary in Settings.instance.PackageLibraries)
			{
				packageLibrary.PackagesChanged += OnPackagesFromLibraryChanged;
				packageLibrary.UpdateTracking();
			}
			await ProviderManager.RefreshProviderAsync<TgzProvider>();
		}

		#region Callbacks
		private void OnPackageSourcesChanged(IReadOnlyList<PackageSource> obj)
		{
			UpdateSourcesTracking();
		}

		private void OnPackageLibrariesChanged(IReadOnlyList<PackageLibrary> obj)
		{
			UpdateLibrariesTracking();
		}

		private async void OnPackagesFromLibraryChanged(PackageLibrary packageLibrary)
		{
			await ProviderManager.RefreshProviderAsync<TgzProvider>();
		}
		#endregion Callbacks
		#endregion Methods
	}
}