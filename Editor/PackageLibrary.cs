/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using LFLRP = LFusion.LocalRegistryProvider;
	using System;
	using System.IO;
	using UnityEngine;

	[Serializable]
	public class PackageLibrary
	{
		#region Fields
		#region Serialized
		[SerializeField] private bool m_isAbsolutePath = false;
		[SerializeField] private string m_path = string.Empty;
		#endregion Serialized

		#region Internal
		private FileSystemWatcher m_watcher = null;
		#endregion Internal
		#endregion Fields

		#region Properties
		public string FullPath
		{
			get
			{
				if (string.IsNullOrEmpty(m_path))
				{
					return null;
				}

				if (m_isAbsolutePath)
				{
					return m_path;
				}
				else
				{
					return System.IO.Path.Combine(LFLRP.Helpers.ProjectPath, m_path);
				}
			}
		}

		public bool IsAbsolutePath
		{
			get => m_isAbsolutePath;
			set
			{
				m_isAbsolutePath = value;
				UpdateTracking();
			}
		}

		public string Path
		{
			get => m_path;
			set
			{
				m_path = value;
				UpdateTracking();
			}
		}
		#endregion Properties

		#region Events
		private Action<PackageLibrary> a_packagesChanged;
		public event Action<PackageLibrary> PackagesChanged
		{
			add
			{
				a_packagesChanged -= value;
				a_packagesChanged += value;
			}
			remove
			{
				a_packagesChanged -= value;
			}
		}
		#endregion Events

		#region Constructor
		public PackageLibrary()
		{
			m_watcher = new FileSystemWatcher()
			{
				IncludeSubdirectories = true,
				Filter = "*.tgz",

				NotifyFilter = NotifyFilters.Attributes
				| NotifyFilters.CreationTime
				| NotifyFilters.DirectoryName
				| NotifyFilters.FileName
				| NotifyFilters.LastWrite
				| NotifyFilters.Security
				| NotifyFilters.Size
			};


			m_watcher.Created += OnWatcherCreated;
			m_watcher.Changed += OnWatcherChanged;
			m_watcher.Deleted += OnWatcherDeleted;
			m_watcher.Renamed += OnWatcherRenamed;
		}
		#endregion Constructor

		#region Finalizer
		~PackageLibrary()
		{
			m_watcher.Dispose();
			m_watcher = null;
		}
		#endregion Finalizer

		#region Methods
		public void UpdateTracking()
		{
			if (IsValideFolder())
			{
				m_watcher.EnableRaisingEvents = false;
				m_watcher.Path = FullPath;
				m_watcher.EnableRaisingEvents = true;
				TiggerPackagesChanged();
			}
		}

		public PackageSource[] GetPackages()
		{
			if (IsValideFolder() == false)
			{
				return null;
			}
			string[] packagePaths = Directory.GetFiles(FullPath, "*.tgz", SearchOption.AllDirectories);

			if (IsAbsolutePath == false)
			{
				for (int i = 0; i < packagePaths.Length; i++)
				{
					packagePaths[i] = packagePaths[i].Replace(LFLRP.Helpers.ProjectPath + "\\", "");
				}
			}

			PackageSource[] output = new PackageSource[packagePaths.Length];
			for (int i = 0; i < packagePaths.Length; i++)
			{
				output[i] = new PackageSource
				{
					Watch = false,
					SourceType = IsAbsolutePath ? SourceType.Local : SourceType.Project,
					Path = packagePaths[i].Replace("\\", "/")
				};
			}

			return output;
		}

		#region Helpers
		private bool IsValideFolder()
		{
			if (string.IsNullOrEmpty(m_path))
			{
				return false;
			}
			return Directory.Exists(FullPath);
		}

		private void TiggerPackagesChanged()
		{
			a_packagesChanged?.Invoke(this);
		}
		#endregion Helpers

		#region Callbacks
		private void OnWatcherCreated(object sender, FileSystemEventArgs e)
		{
			TiggerPackagesChanged();
		}

		private void OnWatcherRenamed(object sender, RenamedEventArgs e)
		{
			TiggerPackagesChanged();
		}

		private void OnWatcherDeleted(object sender, FileSystemEventArgs e)
		{
			TiggerPackagesChanged();
		}

		private void OnWatcherChanged(object sender, FileSystemEventArgs e)
		{
			TiggerPackagesChanged();
		}
		#endregion Callbacks
		#endregion Methods
	}
}