/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider.Helpers
{
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using UnityEngine;
	using UnityEngine.Networking;

	public static class Github
	{
		#region Constants
		private const string GITHUB_API_URL = "https://api.github.com";
		private const string GZIP_CONTENT_TYPE = "application/gzip";
		#endregion Constants

		#region Classes
		#region Parsing
		[Serializable]
		private class ReleaseInfosContainer
		{
			[SerializeField] public ReleaseInfo[] releases;
		}

		[Serializable]
		private class ReleaseInfo
		{
						[SerializeField] public string tag_name;
			[SerializeField] public ReleaseAssetInfo[] assets;
		}

		[Serializable]
		private class ReleaseAssetInfo
		{
			[SerializeField] public string browser_download_url;
			[SerializeField] public string content_type;
		}
		#endregion Parsing

		public class ReleaseVersion
		{
			public string Version { get => k_version; }
			public string Url { get => k_url; }

			private readonly string k_version;
			private readonly string k_url;

			public ReleaseVersion(string version, string url)
			{
				k_version = version;
				k_url = url;
			}
		}
		#endregion Classes

		#region Delegates
		public delegate string VersionParserDelegate(string version);
		#endregion Delegates

		#region Methods
		public static ReleaseVersion[] GetPackagesFromRelease(string owner, string repo, VersionParserDelegate versionParser = null)
		{
			Web.GetResult downloadedValue = Web.Get(GITHUB_API_URL + "/" + "repos" + "/" + owner + "/" + repo + "/" + "releases");
			if (downloadedValue == null)
			{
				return null;
			}

			return ProcessGitHubReleasesData(downloadedValue.Text, versionParser);
		}

		public static async Task<ReleaseVersion[]> GetPackagesFromReleaseAsync(string owner, string repo, VersionParserDelegate versionParser = null)
		{
			Web.GetResult downloadedValue = await Web.GetAsync(GITHUB_API_URL + "/" + "repos" + "/" + owner + "/" + repo + "/" + "releases");
			if (downloadedValue == null)
			{
				return null;
			}

			return ProcessGitHubReleasesData(downloadedValue.Text, versionParser);
		}

		private static ReleaseVersion[] ProcessGitHubReleasesData(string json, VersionParserDelegate versionParser = null)
		{
			// Trick to parse with Unity Json Parser
			json = "{\"releases\":" + json + "}";
			ReleaseInfosContainer releaseInfos = JsonUtility.FromJson<ReleaseInfosContainer>(json);

			List<ReleaseVersion> releaseVersions = new List<ReleaseVersion>();

			foreach (ReleaseInfo releaseInfo in releaseInfos.releases)
			{
				foreach (ReleaseAssetInfo asset in releaseInfo.assets)
				{
					// Validate Version
					string version = releaseInfo.tag_name;
					if (versionParser != null)
					{
						version = versionParser(version);
						if (string.IsNullOrEmpty(version))
						{
							continue;
						}
					}

					// Check if is Tgz
					if (asset.content_type == GZIP_CONTENT_TYPE && asset.browser_download_url.ToLower().EndsWith(".tgz"))
					{
						releaseVersions.Add(new ReleaseVersion(version, asset.browser_download_url));
					}
				}
			}

			return releaseVersions.ToArray();
		}
		#endregion Methods
	}
}