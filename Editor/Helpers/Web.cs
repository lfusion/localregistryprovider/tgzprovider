/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider.Helpers
{
	using System.Threading.Tasks;
	using UnityEngine.Networking;

	public static class Web
	{
		#region Classes
		public class GetResult
		{
			#region Fields
			private readonly byte[] _data;
			private readonly string _text;
			#endregion Fields

			#region Properties
			public byte[] Data
			{
				get => _data;
			}

			public string Text
			{
				get => _text;
			}
			#endregion Properties

			public GetResult(byte[] data, string text)
			{
				_data = data;
				_text = text;
			}
		}
		#endregion Classes

		#region Constants
		private const string LOG_PREFIX = "<color=orange>[Web]</color>";
		#endregion Constants

		#region Methods
		#region Download
		public static GetResult Get(string url)
		{
			using UnityWebRequest webRequest = UnityWebRequest.Get(url);
			Log(string.Format("Retrieving {0}", url));

			// Request and wait for the desired page.
			webRequest.SendWebRequest();

			while (!webRequest.isDone)
			{

			}

			return ProcessWebRequestResult(webRequest);
		}

		public static async Task<GetResult> GetAsync(string url)
		{
			using UnityWebRequest webRequest = UnityWebRequest.Get(url);
			Log(string.Format("Retrieving {0}", url));

			// Request and wait for the desired page.
			webRequest.SendWebRequest();

			while (!webRequest.isDone)
			{
				await Task.Yield();
			}

			return ProcessWebRequestResult(webRequest);
		}

		private static GetResult ProcessWebRequestResult(UnityWebRequest webRequest)
		{
			switch (webRequest.result)
			{
				case UnityWebRequest.Result.ConnectionError:
					LogError(string.Format("Retrieving {0} failed : \nConnection Error : {1}", webRequest.uri, webRequest.error));
					break;
				case UnityWebRequest.Result.DataProcessingError:
					LogError(string.Format("Retrieving {0} failed : \nError : {1}", webRequest.uri, webRequest.error));
					break;
				case UnityWebRequest.Result.ProtocolError:
					LogError(string.Format("Retrieving {0} failed : \nHttp Error : {1}", webRequest.uri, webRequest.error));
					break;
				case UnityWebRequest.Result.Success:

					Log(string.Format("{0} retrieved", webRequest.uri));

					return new GetResult(webRequest.downloadHandler.data, webRequest.downloadHandler.text);
			}

			return null;
		}
		#endregion Download

		#region Debug
		private static void Log(string log)
		{
			Debug.Log(log, LOG_PREFIX);
		}

		private static void LogWarning(string log)
		{
			Debug.LogWarning(log, LOG_PREFIX);
		}

		private static void LogError(string log)
		{
			Debug.LogError(log, LOG_PREFIX);
		}
		#endregion Debug
		#endregion Methods
	}
}