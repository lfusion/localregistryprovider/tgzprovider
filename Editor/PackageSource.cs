﻿/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barbé
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using LFLRP = LFusion.LocalRegistryProvider;
	using System;
	using System.IO;
	using UnityEngine;

	public enum SourceType
	{
		Project,
		Local,
		Http
	}

	[Serializable]
	public class PackageSource
	{
		#region Fields
		#region Serialized
		[SerializeField] private SourceType m_sourceType;
		[SerializeField] private string m_path;
		private bool m_watch = true;
		#endregion Serialized

		#region Internal
		private FileSystemWatcher m_watcher = null;
		#endregion Internal
		#endregion Fields

		#region Properties
		public string FullPath
		{
			get
			{
				if (string.IsNullOrEmpty(m_path))
				{
					return null;
				}

				switch (m_sourceType)
				{
					case SourceType.Project:
						return System.IO.Path.Combine(LFLRP.Helpers.ProjectPath, m_path);
					case SourceType.Local:
					case SourceType.Http:
						return m_path;
					default:
						throw new InvalidDataException();
				}
			}
		}

		public SourceType SourceType
		{
			get => m_sourceType;
			set
			{
				m_sourceType = value;
				UpdateTracking();
			}
		}

		public string Path
		{
			get => m_path;
			set
			{
				m_path = value;
				UpdateTracking();
			}
		}

		public bool Watch
		{
			get => m_watch;
			set
			{
				m_watch = value;
				UpdateTracking();
			}
		}
		#endregion Properties

		#region Events
		private Action<PackageSource> a_packageChanged;
		public event Action<PackageSource> PackageChanged
		{
			add
			{
				a_packageChanged -= value;
				a_packageChanged += value;
			}
			remove
			{
				a_packageChanged -= value;
			}
		}
		#endregion Events

		#region Constructor
		public PackageSource()
		{
			m_watcher = new FileSystemWatcher()
			{
				IncludeSubdirectories = false,
				Filter = "*.tgz",

				NotifyFilter = NotifyFilters.Attributes
				| NotifyFilters.CreationTime
				| NotifyFilters.DirectoryName
				| NotifyFilters.FileName
				| NotifyFilters.LastWrite
				| NotifyFilters.Security
				| NotifyFilters.Size
			};

			m_watcher.Created += OnWatcherCreated;
			m_watcher.Changed += OnWatcherChanged;
			m_watcher.Deleted += OnWatcherDeleted;
			m_watcher.Renamed += OnWatcherRenamed;
		}
		#endregion Constructor

		#region Finalizer
		~PackageSource()
		{
			m_watcher.Dispose();
			m_watcher = null;
		}
		#endregion Finalizer

		#region Methods
		public void UpdateTracking()
		{
			if (m_watch && IsValidePackage() && SourceType != SourceType.Http)
			{
				m_watcher.EnableRaisingEvents = false;
				m_watcher.Path = Directory.GetParent(FullPath).FullName;
				m_watcher.Filter = new FileInfo(FullPath).Name;
				m_watcher.EnableRaisingEvents = true;
				TiggerPackageChanged();
			}
			else
			{
				m_watcher.EnableRaisingEvents = false;
				m_watcher.Path = string.Empty;
			}
		}

		#region Helpers
		public bool IsValidePackage()
		{
			if (string.IsNullOrEmpty(m_path))
			{
				return false;
			}

			FileInfo fileInfo = new FileInfo(FullPath);

			if (fileInfo.Extension.ToLower() != ".tgz")
			{
				return false;
			}

			if (SourceType == SourceType.Http)
			{
				if (Uri.IsWellFormedUriString(Path, UriKind.RelativeOrAbsolute) == false)
				{
					return false;
				}
			}
			else
			{
				if (File.Exists(FullPath) == false)
				{
					return false;
				}
			}

			return true;
		}

		private void TiggerPackageChanged()
		{
			a_packageChanged?.Invoke(this);
		}
		#endregion Helpers

		#region Callbacks
		private void OnWatcherCreated(object sender, FileSystemEventArgs e)
		{
			TiggerPackageChanged();
		}

		private void OnWatcherRenamed(object sender, RenamedEventArgs e)
		{
			TiggerPackageChanged();
		}

		private void OnWatcherDeleted(object sender, FileSystemEventArgs e)
		{
			TiggerPackageChanged();
		}

		private void OnWatcherChanged(object sender, FileSystemEventArgs e)
		{
			TiggerPackageChanged();
		}
		#endregion Callbacks
		#endregion Methods
	}
}