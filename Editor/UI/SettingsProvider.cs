/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider.UI
{
	using System;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;
	using LFLRP = LocalRegistryProvider;
	using LFUI = LocalRegistryProvider.UI;

	static class LFusionTgzProviderSettingsUIElementsRegister
	{
		#region Fields
		private static Button s_applyNowBtn = null;

		private static readonly HashSet<string> s_keywords = new HashSet<string>(new string[]
		{
			"LFusion",
			"Local",
			"Registry",
			"Provider",
			"Packages",
			"Tgz"
		});
		#endregion Fields

		#region Methods
		[SettingsProvider]
		public static SettingsProvider CreateMyCustomSettingsProvider()
		{
			// First parameter is the path in the Settings window.
			// Second parameter is the scope of this setting: it only appears in the Settings window for the Project scope.
			SettingsProvider provider = new SettingsProvider(LFUI.Helpers.SettingsPath(Constants.PackageShortDisplayedName), SettingsScope.Project)
			{
				label = Constants.PackageShortDisplayedName,
				// activateHandler is called when the user clicks on the Settings item in the Settings window.
				activateHandler = (searchContext, rootElement) =>
				{
					VisualElement settingsContainer = LFUI.Helpers.LoadSettingsContainer(rootElement, Constants.PackageDisplayedName, LFLRP.Helpers.GetInstalledPackageVersion(Constants.PackageName));

					SerializedObject settings = Settings.GetSerialized();

					Label descriptionLabel = new Label(Constants.Description);
					descriptionLabel.style.marginBottom = 8;
					descriptionLabel.style.whiteSpace = WhiteSpace.Normal;

					settingsContainer.Add(descriptionLabel);

					Foldout mainFoldout = new Foldout() { name = "packagesListContainer", text = "Packages" };
					mainFoldout.AddToClassList("mb-4");
					settingsContainer.Add(mainFoldout);

					mainFoldout.Add(CreateListElement(
						serializedProperty: settings.FindProperty("m_packageLibraries"), 
						name: "Package Libraries",
						addCallback: Settings.instance.NotifyPackageLibrariesChanged,
						removeCallback: Settings.instance.NotifyPackageLibrariesChanged
						));

					mainFoldout.Add(CreateListElement(settings.FindProperty("m_packageSources"), "Individual Packages"));

					s_applyNowBtn = new Button()
					{
						text = "Apply Now"
					};
					s_applyNowBtn.SetEnabled(ProviderManager.IsRefreshingProvider == false);
					if (ProviderManager.IsRefreshingProvider)
					{
						ProviderManager.IsRefreshingProviderHasChanged += OnIsRefreshingProviderHasChanged;
					}
					s_applyNowBtn.style.alignSelf = Align.FlexEnd;
					s_applyNowBtn.clicked += OnApplyNowBtnClicked;

					mainFoldout.Add(s_applyNowBtn);

					Foldout cacheFoldout = new Foldout()
					{
						text = "Cache Manager"
					};
					cacheFoldout.value = false;

					settingsContainer.Add(cacheFoldout);

					cacheFoldout.Add(new ITgzProviderCacheEditor(ProviderManager.GetProvider<TgzProvider>().GetPackageSources()));

					rootElement.Bind(settings);
				},

				deactivateHandler = () =>
				{
					ProviderManager.IsRefreshingProviderHasChanged -= OnIsRefreshingProviderHasChanged;
					var task = ProviderManager.RefreshProviderAsync<TgzProvider>(); 
				},

				// Populate the search keywords to enable smart search filtering and label highlighting:
				keywords = s_keywords
			};

			return provider;
		}

		private static VisualElement CreateListElement(SerializedProperty serializedProperty, string name = "List", Action addCallback = null, Action removeCallback = null)
		{
			VisualElement listContainer = new VisualElement();

			listContainer.style.borderTopWidth = 1;
			listContainer.style.borderRightWidth = 1;
			listContainer.style.borderBottomWidth = 1;
			listContainer.style.borderLeftWidth = 1;

			listContainer.style.borderTopColor = Color.black;
			listContainer.style.borderRightColor = Color.black;
			listContainer.style.borderBottomColor = Color.black;
			listContainer.style.borderLeftColor = Color.black;

			listContainer.style.marginBottom = 5;

			Toolbar toolbar = new Toolbar();

			Label toolbarLabel = new Label
			{
				text = name
			};

			ToolbarSpacer toolbarSpacer = new ToolbarSpacer();
			toolbarSpacer.style.flexGrow = 1;

			ToolbarButton addBtn = new ToolbarButton()
			{
				text = "+"
			};
			addBtn.AddToClassList("border-right-0");

			ToolbarButton removeBtn = new ToolbarButton()
			{
				text = "-"
			};
			removeBtn.AddToClassList("border-right-0");

			toolbar.Add(toolbarLabel);
			toolbar.Add(toolbarSpacer);
			toolbar.Add(addBtn);
			toolbar.Add(removeBtn);

			listContainer.Add(toolbar);

			ListView listView = new ListView();
			listView.style.height = new StyleLength(250);
			listView.showBoundCollectionSize = false;
			listView.reorderable = true;

			listView.makeItem = () =>
			{
				VisualElement container = new VisualElement();
				container.style.flexDirection = FlexDirection.Row;
				container.style.alignContent = Align.Center;

				PropertyField propertyField = new PropertyField();
				propertyField.style.alignSelf = Align.Center;
				propertyField.style.flexGrow = 1;

				container.Add(propertyField);

				return container;
			};

			listView.bindItem = (e, i) =>
			{
				e.Q<PropertyField>()?.BindProperty(serializedProperty.GetArrayElementAtIndex(i));
			};

			addBtn.clicked += () =>
			{
				serializedProperty.InsertArrayElementAtIndex(serializedProperty.arraySize);
				serializedProperty.serializedObject.ApplyModifiedProperties();
				addCallback?.Invoke();
			};

			removeBtn.clicked += () =>
			{
				if (listView.selectedIndex < 0)
				{
					return;
				}

				serializedProperty.DeleteArrayElementAtIndex(listView.selectedIndex);
				serializedProperty.serializedObject.ApplyModifiedProperties();
				removeCallback?.Invoke();
			};

			listView.BindProperty(serializedProperty);

			listContainer.Add(listView);

			return listContainer;
		}

		#region Callbacks
		private static async void OnApplyNowBtnClicked()
		{
			s_applyNowBtn.SetEnabled(false);
			await ProviderManager.RefreshProviderAsync<TgzProvider>();
			s_applyNowBtn.SetEnabled(true);
		}
		private static void OnIsRefreshingProviderHasChanged(bool isRefreshing)
		{
			s_applyNowBtn.SetEnabled(isRefreshing == false);
			if (isRefreshing == false)
			{
				ProviderManager.IsRefreshingProviderHasChanged -= OnIsRefreshingProviderHasChanged;
			}
		}
		#endregion Callbacks
		#endregion Methods
	}
}
