﻿/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barbé
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using System.Collections.Generic;
	using System.Linq;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;

	public partial class ITgzProviderCacheEditor : VisualElement
	{
		#region Fields
		IEnumerable<PackageSource> m_packageSourcesFilter = null;
		ToolbarSearchField m_searchField = new ToolbarSearchField();
		#endregion Fields

		#region Constructor
		public ITgzProviderCacheEditor(IEnumerable<PackageSource> packageSourcesFilter = null)
		{
			m_packageSourcesFilter = packageSourcesFilter;

			m_searchField.RegisterValueChangedCallback(OnSearchChanged);

			Draw();
			ProviderManager.IsRefreshingProviderHasChanged += OnIsRefreshingProviderHasChanged;
			SetEnabled(ProviderManager.IsRefreshingProvider == false);
		}
		#endregion Constructor

		#region Finalizer
		~ITgzProviderCacheEditor()
		{
			ProviderManager.IsRefreshingProviderHasChanged -= OnIsRefreshingProviderHasChanged;
			Clear();
		}
		#endregion Finalizer

		#region Methods
		private void Search(string query)
		{
			foreach (VisualElement item in Children())
			{
				if (item is CacheSourcesGroupEditor group)
				{
					if (string.IsNullOrEmpty(query))
					{
						group.style.display = DisplayStyle.Flex;
					}
					else
					{
						group.style.display = group.Label.ToLower().Contains(query.ToLower()) ? DisplayStyle.Flex : DisplayStyle.None;
					}
				}
			}
		}

		private void Draw()
		{
			Dictionary<string, PackageVersionInfo> cache = ITgzProvider.Cache;

			IEnumerable<string> keys = cache.Keys;

			if (m_packageSourcesFilter != null)
			{
				keys = keys.Where(key => m_packageSourcesFilter.Any(x => x.FullPath == key));
			}

			var grouped = keys.GroupBy(
					x => ITgzProvider.Cache[x].Name,
					x => x,
					(key, g) => new KeyValuePair<string, List<string>> (key, g.ToList()) 
				)
				.OrderBy(x => x.Key);

			style.borderLeftWidth = 1;
			style.borderRightWidth = 1;
			style.borderBottomWidth = 1;
			style.borderTopWidth = 1;

			style.borderRightColor = Color.black;
			style.borderLeftColor = Color.black;
			style.borderTopColor = Color.black;
			style.borderBottomColor = Color.black;

			Toolbar toolbar = new Toolbar();

			Label label = new Label("Cached Packages");

			ToolbarSpacer toolbarSpacer = new ToolbarSpacer();
			toolbarSpacer.style.flexGrow = 1;

			ToolbarButton saveNowButton = new ToolbarButton()
			{
				text = "Save now"
			};
			saveNowButton.clicked += () => ITgzProvider.SaveCache();

			toolbar.Add(label);
			toolbar.Add(toolbarSpacer);
			toolbar.Add(m_searchField);

			Add(toolbar);


			foreach (var version in grouped)
			{
				Add(new CacheSourcesGroupEditor(version.Key, version.Value));
			}
			Search(m_searchField.value);
		}

		internal static void ConfigDeleteButtonStyle(ref Button button)
		{
			button.style.unityFontStyleAndWeight = FontStyle.Bold;
			button.style.marginTop = 2;

			Color deleteRed = new Color(0.8f, 0f, 0f);

			button.style.borderTopColor = deleteRed;
			button.style.borderBottomColor = deleteRed;
			button.style.borderLeftColor = deleteRed;
			button.style.borderRightColor = deleteRed;

			button.RegisterCallback<ClickEvent>(x => { x.StopPropagation(); });
		}

		internal static void DeleteFromCache(string key)
		{
			ITgzProvider.Cache.Remove(key);
		}

		internal static void DeleteFromCache(List<string> keys)
		{
			foreach (string key in keys)
			{
				DeleteFromCache(key);
			}
		}

		#region Callbacks
		private void OnSearchChanged(ChangeEvent<string> changeEvent)
		{
			Search(changeEvent.newValue);
		}

		private void OnIsRefreshingProviderHasChanged(bool providerIsRefreshing)
		{
			SetEnabled(providerIsRefreshing == false);
			if (providerIsRefreshing == false)
			{
				Clear();
				Draw();
			}
		}
		#endregion Callbacks
		#endregion Methods
	}
}