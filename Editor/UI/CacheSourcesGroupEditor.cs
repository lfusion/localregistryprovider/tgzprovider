﻿/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barbé
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine;
	using UnityEngine.UIElements;
	using LFUI = LFusion.LocalRegistryProvider.UI;

	public partial class ITgzProviderCacheEditor
	{
		internal class CacheSourcesGroupEditor : LFUI.Foldout
		{
			#region Fields
			private List<string> m_sourceKeys;
			private Button m_removeButton;
			#endregion Fields

			#region Constructor
			internal CacheSourcesGroupEditor(string label, List<string> contentKeys)
			{
				style.marginLeft = 1;
				style.marginRight = 1;

				Label = label;
				m_sourceKeys = contentKeys;

				m_removeButton = new Button()
				{
					text = "X"
				};

				ConfigDeleteButtonStyle(ref m_removeButton);
				m_removeButton.clicked += OnRemoveButtonClicked;

				headerContainer.Add(m_removeButton);

				Draw();
			}
			#endregion Constructor

			#region Finalizer
			~CacheSourcesGroupEditor()
			{
				m_removeButton.clicked -= OnRemoveButtonClicked;
				Clear();
			}
			#endregion Finalizer

			#region Methods
			internal void Draw()
			{
				int i = 0;
				foreach (var key in m_sourceKeys)
				{
					if (ITgzProvider.Cache.ContainsKey(key) == false)
					{
						continue;
					}
					CacheSourceEditor item = new CacheSourceEditor(key);

					if (i % 2 > 0)
					{
						item.style.backgroundColor = new Color(0.5f, 0.5f, 0.5f, 0.15f);
					}
					item.CacheRemoved += OnCacheRemoved;

					Add(item);
					i++;
				}

				if (i < 1)
				{
					parent.Remove(this);
				}
			}

			internal new void Clear()
			{
				foreach (CacheSourceEditor child in Children())
				{
					child.CacheRemoved -= OnCacheRemoved;
				}
				base.Clear();
			}

			#region Callbacks
			private void OnCacheRemoved(CacheSourceEditor obj)
			{
				Clear();
				Draw();
			}

			private void OnRemoveButtonClicked()
			{
				string title = "Are you sure?";
				string message = "You will remove cache for all versions of " + Label + "package.\nAre you sure?";
				if (EditorUtility.DisplayDialog(title, message, "Yes", "No"))
				{
					DeleteFromCache(m_sourceKeys);
					parent.Remove(this);
					ITgzProvider.SaveCache();
				}
			}
			#endregion Callbacks
			#endregion Methods
		}
	}
}