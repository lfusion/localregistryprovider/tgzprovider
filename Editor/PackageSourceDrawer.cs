/*
*	LFusion Tgz Provider is an extension for LFusion Local Registry Provider.  
*	It can be used to reference tgz packages stored in the project, locally or online.  
*	It also contains classes and helpers to simplify the implementation of providers.  
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.TgzProvider
{
	using LFLRP = LFusion.LocalRegistryProvider;
	using System.IO;
	using UnityEditor;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;

	[CustomPropertyDrawer(typeof(PackageSource))]
	public class PackageSourceDrawer : PropertyDrawer
	{
		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			return new PackageSourceVisualElement(property);
		}
	}

	public class PackageSourceVisualElement : VisualElement
	{
		private const string SOURCE_TYPE_NAME = "m_sourceType";
		private const string PATH_NAME = "m_path";

		private Label m_pathLabel = null;
		private PropertyField m_pathField = null;
		private Button m_selectBtn = null;
		private SerializedProperty m_property = null;

		public PackageSourceVisualElement(SerializedProperty property)
		{
			m_property = property;

			style.flexDirection = FlexDirection.Row;

			Label labelType = new Label()
			{
				text = "SourceType"
			};

			StyleSheet disablePropertyLabelStyle = LFLRP.UI.Helpers.LoadDisablePropertyLabelStyle();

			PropertyField modeField = new PropertyField(property.FindPropertyRelative(SOURCE_TYPE_NAME));
			modeField.styleSheets.Add(disablePropertyLabelStyle);
			modeField.RegisterValueChangeCallback(OnTypeChanged);

			m_pathLabel = new Label()
			{
				text = GetPathLabel((SourceType)property.FindPropertyRelative(SOURCE_TYPE_NAME).enumValueIndex)
			};

			m_pathField = new PropertyField(property.FindPropertyRelative(PATH_NAME));
			m_pathField.styleSheets.Add(disablePropertyLabelStyle);
			m_pathField.style.flexGrow = 1;

			m_pathField.RegisterValueChangeCallback(OnPathChanged);

			m_selectBtn = new Button()
			{
				text = "Select"
			};

			m_selectBtn.clicked += OnSelectBtnClicked;

			Add(labelType);
			Add(modeField);
			Add(m_pathLabel);
			Add(m_pathField);
			Add(m_selectBtn);
		}

		private void OnPathChanged(SerializedPropertyChangeEvent e)
		{
			UpdateUI();

			PackageSource packageSource = LFLRP.Helpers.GetTargetObjectOfProperty(m_property) as PackageSource;
			packageSource?.UpdateTracking();
		}

		private void UpdateUI()
		{
			SourceType sourceType = (SourceType)m_property.FindPropertyRelative(SOURCE_TYPE_NAME).enumValueIndex;
			string path = m_property.FindPropertyRelative(PATH_NAME).stringValue;
			string error = CheckPath(sourceType, path);

			m_pathLabel.text = GetPathLabel(sourceType);

			SetButtonEnabled(sourceType);

			if (error != null)
			{
				AddToClassList("error");
				style.backgroundColor = new Color(1, 0, 0, 0.35f);
				tooltip = error;
			}
			else
			{
				RemoveFromClassList("error");
				style.backgroundColor = new StyleColor();
				tooltip = null;
			}
		}

		private void OnSelectBtnClicked()
		{
			string absolutePath = EditorUtility.OpenFilePanel("Select Tgz Package", LFLRP.Helpers.ProjectPath, "tgz");

			SourceType sourceType = (SourceType)m_property.FindPropertyRelative(SOURCE_TYPE_NAME).enumValueIndex;

			if (sourceType == SourceType.Local)
			{
				m_property.FindPropertyRelative(PATH_NAME).stringValue = absolutePath;
				m_property.serializedObject.ApplyModifiedProperties();
			}
			else if (sourceType == SourceType.Project)
			{
				m_property.FindPropertyRelative(PATH_NAME).stringValue = absolutePath.Replace(LFLRP.Helpers.ProjectPath, "").Remove(0, 1);
				m_property.serializedObject.ApplyModifiedProperties();
			}
		}

		private void OnTypeChanged(SerializedPropertyChangeEvent e)
		{
			UpdateUI();

			PackageSource packageSource = LFLRP.Helpers.GetTargetObjectOfProperty(m_property) as PackageSource;
			packageSource?.UpdateTracking();
		}

		private string GetPathLabel(SourceType source)
		{
			switch (source)
			{
				case SourceType.Project: return "Relative Path";
				case SourceType.Local: return "Absolute Path";
				case SourceType.Http: return "Url";
			}

			return null;
		}

		private void SetButtonEnabled(SourceType source)
		{
			switch (source)
			{
				case SourceType.Project:
					m_selectBtn.SetEnabled(true);
					break;
				case SourceType.Local:
					m_selectBtn.SetEnabled(true);
					break;
				case SourceType.Http:
					m_selectBtn.SetEnabled(false);
					break;
			}
		}

		private string CheckPath(SourceType source, string path)
		{
			if (string.IsNullOrEmpty(path))
			{
				return "Path is empty";
			}

			if (source == SourceType.Http)
			{
				return null;
			}

			if (source == SourceType.Project)
			{
				path = LFLRP.Helpers.ProjectPath + "/" + path;
			}

			if (Directory.GetParent(path).Exists == false || File.Exists(path) == false)
			{
				return "File not found";
			}

			FileInfo fileInfo = new FileInfo(path);

			if (fileInfo.Extension.ToLower() != ".tgz")
			{
				return "Is not a tgz file";
			}

			return null;
		}
	}
}